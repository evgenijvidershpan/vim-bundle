function! s:GetDefinitionInfo()
  exe 'normal dma'
  exe 'normal ma'
  " Get class
  call search('^\s*\<class\>', 'b')
  exe 'normal ^w"aY'
  let s:class = substitute(@a, '^\s*class\>.\{-}\(\w\+\)\s*\([:{]\|$\).*', '\1', '')
  let l:ns = search('^\s*\<namespace\>', 'b')
  " Get namespace
  if l:ns != 0
    exe 'normal ^w"ayw'
    let s:namespace = @a
  else
    let s:namespace = ''
  endif
  " Go back to definition
  exe 'normal `a'
  exe 'normal "aY'
  let s:defline = substitute(@a, ';\n', '', '')
  exe 'normal dma'
endfunction

function! s:ImplementDefinition()
  call append('.', s:defline)
  exe 'normal j'
  " Remove keywords
  s/\<virtual\>\s*//e
  s/\<static\>\s*//e
  if s:namespace == ''
    let l:classString = s:class . "::"
  else
    let l:classString = s:namespace . "::" . s:class . "::"
  endif
  " Remove default parameters
  s/\s\{-}=\s\{-}[^,)]\{1,}//e
  " Add class qualifier
  exe 'normal ^f(bi' . l:classString
  " Add brackets
  exe "normal $o{\<CR>\<TAB>\<CR>}\<CR>\<ESC>kkkk"
  " Fix indentation
  exe 'normal =4j^'
endfunction

command! CopyDefinition :call s:GetDefinitionInfo()
command! ImplementDefinition :call s:ImplementDefinition()
command! InsertDefinition :call s:ImplementDefinition()

function! s:join(array)
    let l:rvl = '['
    for i in a:array
        let l:rvl = l:rvl . i . ', '
    endfor
    return substitute(l:rvl, '[, ]*$', ']', '')
endfunction

function! s:olsplit(str)
    " если имя оканчивается на цифру, нам нужен маркер который будет заведомо
    " меньше по значению любой строки-разделителя нумерованного списка, причём
    " добавляем маркер в конец имени, перед расширением если имеется.
    let name = substitute(a:str, '\(\.\d*\|\zs\)\(\.\w*\|\ze\)$', "\\1\x01\\2", '')

    let chunks = split(name, '\([_.-]\+\|\D\+\|\d\+\)\zs')
    if len(chunks) < 1
        return chunks
    endif
    " имя начинается с цифры? - помещаем маркер для смещения к \x2E (точка)
    if match(chunks[0], '^\d') ==# 0
        " для разделения разных типов списков не имеющих префикса
        if len(chunks) ># 1
            call insert(chunks, chunks[1], 0)
            call insert(chunks, ':', 0)
        endif
        call insert(chunks, "\x2F", 0)
    endif
    let i = 0
    while i < len(chunks)
        if match(chunks[i], '^\d') ==# 0
            let chunks[i] = str2nr(chunks[i])
        endif
        let i = i + 1
    endwhile
    return chunks
endfunction

function! s:compare(a, b) abort
    for i in range(1, min([len(a:a), len(a:b)])-1)
        if type(a:a[i]) ==# type(a:b[i])
            if a:a[i] <# a:b[i]
                return - 1
            elseif a:a[i] ># a:b[i]
                return 1
            endif
        elseif type(a:a[i]) ==# v:t_number
            return a:b[i] ># '9'? 1 : -1
        elseif type(a:b[i]) ==# v:t_number
            return a:a[i] <# '0'? 1 : -1
        endif
    endfor
    if len(a:a) < len(a:b)
        return -1
    elseif len(a:a) > len(a:b)
        return 1
    endif
    return 0
endfunction

function! s:outlinesort() range
    let l:srt = []
    for l:n in range(a:firstline, a:lastline)
        let l:ln = getline(l:n)
        call add(l:srt, extend([l:ln], s:olsplit(l:ln)))
    endfor
    call sort(l:srt, function('s:compare'))
    let i = 0
    for l:n in range(a:firstline, a:lastline)
        call setline(l:n, l:srt[i][0])
        let i = i + 1
    endfor
endfunction

function! s:outlinesortdebug() range
    let l:srt = []
    for l:n in range(a:firstline, a:lastline)
        let l:ln = getline(l:n)
        call add(l:srt, extend([l:ln], s:olsplit(l:ln)))
    endfor
    call sort(l:srt, function('s:compare'))
    let i = 0
    for l:n in range(a:firstline, a:lastline)
        call setline(l:n, s:join(l:srt[i]))
        let i = i + 1
    endfor
endfunction

function s:shuffle(list)
  " Fisher-Yates-Durstenfeld-Knuth
  let n = len(a:list)
  for i in range(0, n-2)
    let j = Random(0, n-i-1)
    let e = a:list[i]
    let a:list[i] = a:list[i+j]
    let a:list[i+j] = e
  endfor
  return a:list
endfunction

function! s:shufflestrings() range
    let l:srt = []
    for l:n in range(a:firstline, a:lastline)
        call add(l:srt, getline(l:n))
    endfor
    call s:shuffle(l:srt)
    let i = 0
    for l:n in range(a:firstline, a:lastline)
        call setline(l:n, l:srt[i])
        let i = i + 1
    endfor
endfunction

" -----------------------------------------
" https://github.com/tpope/vim-unimpaired
" -----------------------------------------
function! extra#url_encode(str) abort
  " iconv trick to convert utf-8 bytes to 8bits indiviual char.
  return substitute(iconv(a:str, 'latin1', 'utf-8'),'[^A-Za-z0-9_.~-]','\="%".printf("%02X",char2nr(submatch(0)))','g')
endfunction

function! extra#url_decode(str) abort
  let str = substitute(substitute(substitute(a:str,'%0[Aa]\n$','%0A',''),'%0[Aa]','\n','g'),'+',' ','g')
  return iconv(substitute(str,'%\(\x\x\)','\=nr2char("0x".submatch(1))','g'), 'utf-8', 'latin1')
endfunction
" -----------------------------------------


function! extra#transform(first, last, func) abort
    for l:n in range(a:first, a:last)
        call setline(l:n, a:func(getline(l:n)))
    endfor
endfunction

function! extra#mdresyn()
  " Получаем содержимое текущего буфера
  let content = getline(1, '$')
  let langs = []

  " Ищем языки в блоках кода и добавляем их в список
  for line in content
    if line =~# '\v^\s*```\w+$'
      let lang = substitute(line, '\v^\s*```(\w+)$', '\1', '')
      call add(langs, lang) " Добавляем язык в список
    endif
  endfor

  " Удаляем дубликаты из списка языков
  let langs = uniq(langs)

  " Загружаем синтаксис для каждого языка
  for lang in langs
    unlet b:current_syntax
    silent! exec printf("syntax include @%s syntax/%s.vim", lang, lang)
    exec printf("syntax region %sSnip matchgroup=Snip start='```%s' end='```' contains=@%s",
                \ lang, lang, lang)
  endfor
  let b:current_syntax='mkd'
  syntax sync fromstart
endfunction


function! extra#refactory()
  let @/='\V\<'.escape(expand('<cword>'), '\').'\>'
  set hls
  exe 'normal [{V]}'
endfunction


let s:seq_step = 1

function! SeqStart(start)
    let s:seq_step = a:start
endfunction

function! SeqMul(to)
  let l:rv = s:seq_step
  let s:seq_step = s:seq_step * a:to
  return l:rv
endfunction

function! SeqBinary()
  return SeqMul(2)
endfunction


command MDResyntax call extra#mdresyn()
command Refactory call extra#refactory()

command -range=% OutlineSort <line1>,<line2>call s:outlinesort()
command -range=% OutlineSortDebug <line1>,<line2>call s:outlinesortdebug()
command -range=% Shuffle <line1>,<line2>call s:shufflestrings()

command -range UrlEncode call extra#transform(<line1>,<line2>,function('extra#url_encode'))
command -range UrlDecode call extra#transform(<line1>,<line2>,function('extra#url_decode'))

" фиксы для [g]vim (unzip из GOW)
if has('win32') && !has('win32unix')
    let $PATH = $PATH . ';' . expand('<sfile>:p:h') . '/!settings/apps'
    if !has('gui')
        colorscheme pablo
    endif
endif

" добавляем поисковую систему global (www.gnu.org/software/global/global.html)
let $PATH = $PATH . ';' . $PAD . '/SYS/global/bin'
let $GTAGSCONF = $PAD . '/SYS/global/share/gtags/gtags.conf'
let $GTAGSLABEL = 'new-ctags'


" let g:ConqueTerm_EscKey = 
let g:ConqueTerm_PyVersion = 3
let g:ConqueTerm_Color = 1
let g:ConqueTerm_InsertOnEnter = 1
let g:ConqueTerm_CWInsert = 1
let g:ConqueTerm_StartMessages = 0
let g:ConqueTerm_ShowBell = 0
let g:ConqueTerm_ReadUnfocused = 1

if has('win32') && !has('win32unix')
    let g:ConqueTerm_PyExe = 'C:/python3.6/python.exe'
endif

" autocmd BufEnter * if &ft == 'conque_term' | * cmd * | endif

let g:snipMate = { 'snippet_version' : 1 }

nnoremap <SPACE> :nohl<CR><SPACE>



" по двойному клику открывать ошибку в редакторе

function extra#conque_handle()
    let l:sattr = synIDattr(synID(line("."), col("."), 1), "name")
    if l:sattr != 'Error'
        " echo 'non-error sentence!'
    endif
    let l:efmt = substitute(expand('<cWORD>'), "\\", '/', 'g')
    let l:mt = matchlist(l:efmt, '\v(.*):(\d+):(\d+):')

    if len(l:mt) != 0
        let l:fn = substitute(l:mt[1], "\\", '/', 'g')
        if !filereadable(l:fn)
            echo 'file not found: ' . l:fn
            return
        endif
        exe "normal \<C-W>p"
        exe "e +" . l:mt[2] . " " . l:fn
        call cursor(l:mt[2], l:mt[3])
       return
    endif
    echo 'error format! must be: "{FN}:{LN}:{COL}: {TYPE}: [msg]"'
    call HighlightWord()
endfunction


function extra#conque_next(i, rev)
    let l:re = ['\v(\w:|[/\\])(.*):(\d+):(\d+): \w+:','\v^(\w:|[/\\])[^>]*\>']
    let @/=l:re[a:i]
    nohl
    if a:rev
        exe 'normal N'
        return
    endif
    exe 'normal n'
endfunction


function extra#conque_toggle()
    if !exists('b:conq_term')
        let b:conq_term = conque_term#open('cmd', ['belowright split', 'resize 10'], 1)
        call b:conq_term.focus()
        return
    endif
    let save_sb = &switchbuf
    sil set switchbuf=useopen
    sil exe 'belowright sb ' . b:conq_term.buffer_number
    sil exe ":set switchbuf=" . save_sb
    resize 10
endfunction


function extra#highlight_errors()
    " подсветка ошибок компиляции в консоли
    syntax match Error "\v(\w:|[/\\]).*:\d+:\d+: \w+:"
    " подцветка prompt
    syntax match Exception "\v^(\w:|[/\\])[^>]*\>"
    nnoremap <silent> <buffer> <2-LeftMouse> :call extra#conque_handle()<cr>
    nnoremap <silent> <buffer> <CR> :call extra#conque_handle()<cr>
endfunction


function extra#conque_setup(term)
    " shrink window height to 10 rows
    resize 10
    " silly example of terminal api usage
    if a:term.program_name == 'cmd'
        call extra#highlight_errors()
    endif
    nnoremap <silent> <buffer> <C-N> :call extra#conque_next(0, 0)<cr>
    nnoremap <silent> <buffer> <C-P> :call extra#conque_next(0, 1)<cr>
    nnoremap <silent> <buffer> <C-O> :call extra#conque_next(1, 1)<cr>
    noremap <silent> <buffer> <C-F9> :hide<cr>
endfunction

call conque_term#register_function('after_startup', 'extra#conque_setup')

nnoremap <silent> <buffer> <C-F9> :call extra#conque_toggle()<cr>


" :set completefunc=extra#CompleteExample
function! extra#CompleteExample(findstart, base)
    if a:findstart
        let l:line = getline('.')
        let l:start = col('.') - 1
        while l:start > 0 && (l:line[l:start - 1] =~ '\a' || l:line[l:start - 1] =~ '.' || l:line[l:start - 1] =~ '-')
            let l:start -= 1
        endwhile
        return l:start
    else
        let l:res = []
        for m in ["a-complete","b-complete"]
            if m =~ '^' . a:base
                call add(l:res, m)
            endif
        endfor
        return l:res
    endif
endfun


function! extra#FixMD()
    sil exe "%s/\\V\\^>\\s\\*/"
    sil exe "%s/\\V\\%xa0/ /g"
    sil exe "15,$s/  / /g"
    sil exe "%s/\\V\\s\\+\\(.\\|,\\)/\\1/g"
    sil exe "%s/\\V!\\([\\[^]]\\+]\\)(\\[^(]\\+)//g"
    sil exe "%s/\\V\\([\\[^]]\\+]\\)(\\[^(]\\+)\\s\\*\\((\\[^)]\\+)\\)/[\\1] \\2/g"
    sil exe "%s/\\v\\[([^\\]]{-})\\]\\((\\S+)\\s*(\"[^\"]*\"\\)|\\))/[[\\1]]/g"
    sil exe "15,$s/\\V**\\s\\*\\([[\\.\\{-}]]\\.\\{-}\\)**/\\1/g"
endfun



" если имеется установленный clang - регистрируем его как lsp
let $PATH = $PATH . ';' . $PAD . '/DEV/clang/bin'

if executable('clangd')
    augroup lsp_clangd
        autocmd!
        autocmd User lsp_setup call lsp#register_server({
                    \ 'name': 'clangd',
                    \ 'cmd': {server_info->['clangd','--compile-commands-dir=./build/clang','--header-insertion=never','--function-arg-placeholders']},
                    \ 'whitelist': ['c', 'cpp', 'objc', 'objcpp'],
                    \ 'initialization_options': {
                    \     'buildDirectory': 'BUILD/clang',
                    \  }
                    \ })
    augroup end
endif


" python -m pip install --upgrade pip
" pip install -U setuptools
" pip install 'python-language-server'

if executable('pyls')
    augroup lsp_pylsp
        autocmd!
        " pip install python-lsp-server
        au User lsp_setup call lsp#register_server({
            \ 'name': 'pyls',
            \ 'cmd': {server_info->['pyls']},
            \ 'whitelist': ['python','py'],
            \ })
    augroup end
endif


" функция настраивающая буффер с lsp-совместимым содержимым (код)
function! s:on_lsp_buffer_enabled() abort
    setlocal omnifunc=lsp#complete
    setlocal signcolumn=yes
    setlocal completeopt-=preview
    setlocal keywordprg=:LspHover

    if exists('+tagfunc') | setlocal tagfunc=lsp#tagfunc | endif
    nmap <buffer> gd <plug>(lsp-definition)
    nmap <buffer> gs <plug>(lsp-document-symbol-search)
    nmap <buffer> gS <plug>(lsp-workspace-symbol-search)
    nmap <buffer> gr <plug>(lsp-references)
    nmap <buffer> gi <plug>(lsp-implementation)
    nmap <buffer> gt <plug>(lsp-type-definition)
    nmap <buffer> <leader>rn <plug>(lsp-rename)
    nmap <buffer> [g <plug>(lsp-previous-diagnostic)
    nmap <buffer> ]g <plug>(lsp-next-diagnostic)
    nmap <buffer> K <plug>(lsp-hover)
    nnoremap <buffer> <expr><c-f> lsp#scroll(+4)
    nnoremap <buffer> <expr><c-d> lsp#scroll(-4)

    let g:lsp_format_sync_timeout = 1000
    " autocmd! BufWritePre *.rs,*.go call execute('LspDocumentFormatSync')
endfunction

" по событию включения lsp для буффера - вызываем функцию настройки буффера
augroup lsp_install
    au!
    " call s:on_lsp_buffer_enabled only for languages that has the server registered.
    autocmd User lsp_buffer_enabled call s:on_lsp_buffer_enabled()
augroup end

if has('terminal')
    tnoremap <F4> <C-W>N<C-W>:call extra#highlight_errors()<CR>
endif

if !exists(":DiffOrig")
  command DiffOrig vert new | set bt=nofile | r ++edit # | 0d_ | diffthis
		  \ | wincmd p | diffthis
endif


function! extra#hex_to_rgb(hex)
    if len(a:hex) == 6
        let l:r = str2nr(a:hex[0:1], 16)
        let l:g = str2nr(a:hex[2:3], 16)
        let l:b = str2nr(a:hex[4:6], 16)
    elseif len(hex) == 3
        let l:r = str2nr(a:hex[0:1], 16) * 17
        let l:g = str2nr(a:hex[1:2], 16) * 17
        let l:b = str2nr(a:hex[2:3], 16) * 17
    else
        throw 'wrong hex format!'
    endif
    return printf('rgb(%d, %d, %d)', l:r, l:g, l:b)
endfunction


function! extra#hex_to_colorref(hex)
    if len(a:hex) == 6
        let l:r = str2nr(a:hex[0:1], 16)
        let l:g = str2nr(a:hex[2:3], 16)
        let l:b = str2nr(a:hex[4:6], 16)
    elseif len(hex) == 3
        let l:r = str2nr(a:hex[0:1], 16) * 17
        let l:g = str2nr(a:hex[1:2], 16) * 17
        let l:b = str2nr(a:hex[2:3], 16) * 17
    else
        throw 'wrong hex format!'
    endif
    return printf('0x%02X%02X%02X', l:b, l:g, l:r)
endfunction


function! s:to_rgb() range
    " Применяем замену в указанном диапазоне
    execute a:firstline . "," . a:lastline . " s/\\v#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})/\\=extra#hex_to_rgb(submatch(1))/g"
endfunction

function! s:to_colorref() range
    " Применяем замену в указанном диапазоне
    execute a:firstline . "," . a:lastline . " s/\\v#([0-9a-fA-F]{6}|[0-9a-fA-F]{3})/\\=extra#hex_to_colorref(submatch(1))/g"
endfunction

command! -range=% XTORGB <line1>,<line2>call s:to_rgb()
command! -range=% XTOCOLOR <line1>,<line2>call s:to_colorref()


function! s:fix_gpt() range
    " Применяем замену в указанном диапазоне
    execute a:firstline . "," . a:lastline . " s@class=\"D4hR0tobSBRm_mZ4hrlS\">@\\0<pre>@g"
    execute a:firstline . "," . a:lastline . " s@</code></div></div></pre>@</pre>\\0@g"
    execute a:firstline . "," . a:lastline . " s@yGEuosa_aZeFroGMfpgu\">@\\0<blockquote>@g"
endfunction

command! -range=% FIXGPT <line1>,<line2>call s:fix_gpt()


let g:templates_directory = expand('<sfile>:p:h') . '/!settings/templates/html.t'

function! MakeHTML()
    if &modified || line('$') > 1 || getline(1) != ''
        new
    endif
    execute '0r ' . g:templates_directory
    set filetype=html
    execute '/<body>'
    normal! j
endfunction


anoremenu <silent> PopUp.Extra.Make\ Html :call MakeHTML()<CR>
anoremenu <silent> PopUp.Extra.Put\ Html :put =system(\"cliphtml\")<CR>

