setlocal completefunc=syntaxcomplete#Complete

function! CMakeGotoFile()
    let l:filename = expand('<cfile>')
    if l:filename =~ '\${CMAKE_CURRENT_SOURCE_DIR}'
        let l:filename = substitute(l:filename, '\${CMAKE_CURRENT_SOURCE_DIR}', expand('%:p:h'), '')
    endif
    " Используем fnamemodify для обработки пути
    let l:filename = fnamemodify(l:filename, ':p')
    execute 'edit' l:filename
endfunction



nnoremap <buffer> gf :call CMakeGotoFile()<CR>

