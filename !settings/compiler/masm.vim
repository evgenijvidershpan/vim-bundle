
if exists("current_compiler")
  finish
endif
let current_compiler = "masm"

let s:cpo_save = &cpo
set cpo&vim

CompilerSet errorformat&
CompilerSet makeprg=makeit.bat

setlocal makeencoding=cp866

let &cpo = s:cpo_save
unlet s:cpo_save
