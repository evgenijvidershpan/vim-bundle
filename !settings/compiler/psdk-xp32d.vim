
if exists("current_compiler")
  finish
endif
let current_compiler = "masm"

let s:cpo_save = &cpo
set cpo&vim

CompilerSet errorformat&

setlocal makeprg=cmd.exe\ /c\ \"call\ c:\\VC2003\\vcvars.bat\ &&\ call\ c:\\PSDK\\SetEnv.Cmd\ /XP32\ /DEBUG\ &&\ nmake\ OUTDIR=BUILD\\PSDKD\ $*\"
setlocal makeencoding=cp866

let &cpo = s:cpo_save
unlet s:cpo_save
