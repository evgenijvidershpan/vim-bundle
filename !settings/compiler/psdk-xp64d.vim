
if exists("current_compiler")
  finish
endif
let current_compiler = "masm"

let s:cpo_save = &cpo
set cpo&vim

CompilerSet errorformat&

setlocal makeprg=cmd.exe\ /c\ \"call\ c:\\PSDK\\SetEnv.Cmd\ /XP64\ /DEBUG\ &&\ nmake\ OUTDIR=BUILD\\PSDK64D\ $*\"
setlocal makeencoding=cp866

let &cpo = s:cpo_save
unlet s:cpo_save
