
if exists("current_compiler")
  finish
endif
let current_compiler = "masm"

let s:cpo_save = &cpo
set cpo&vim

CompilerSet errorformat&

setlocal makeprg=cmd.exe\ /c\ \"call\ c:\\VC98\\VC98VARS.BAT\ &&\ nmake\ /NOLOGO\ APPVER=5.0\ NODEBUG=1\ OUTDIR=BUILD\\VC98\ $*\"
setlocal makeencoding=cp866

let &cpo = s:cpo_save
unlet s:cpo_save
