set nocompatible " отключить режим совместимости с классическим Vi
filetype off

set encoding=utf-8  " основная кодировка VIM
set fileencodings=ucs-bom,utf-8,cp1251  " кодировки открываемых файлов
set termencoding=utf-8 " кодировка вывода на терминал

" по рекомендациям pathogen отключаем сохранение опций
set sessionoptions-=options
set viewoptions-=options

" загружаем pathogen
source /home/vim-bundle/vim-pathogen/autoload/pathogen.vim
" загружаем все плагины
execute pathogen#infect('/home/vim-bundle/{}')
" включем плагины
syntax on
filetype plugin indent on

" {{{ NERDTREE

let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'
let g:NERDTreeShowBookmarks = 1
" т.к. мы будем использовать плагин rooter нам гораздо важнее видеть реальную
" текущую рабочую директорию которая будет стакаться со строкой VIM
let g:NERDTreeStatusline = '%{getcwd()}'

" проверка на то что nerdtree уже открыт
function! IsNTOpen()
    return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

" aвто-выход из VIM если закрыт последний буфер при открытом nerdtree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" переключение окошка nerdtree с переходом к текущему файлу
function! MyToggleNerdTree() abort
  NERDTreeToggleVCS
  if IsNTOpen()
    wincmd p
    NERDTreeFind
  endif
endfunction

" авто-открытие nerdtree при запуске VIM
" autocmd vimenter * :call MyToggleNerdTree()

" по <F2> переключаем окошко nerdtree с авто-переходом в VCS
map <F2> :call MyToggleNerdTree()<CR>

" по клавише <Tab> в nerdtree возврат к предыдущему активному окошку
autocmd FileType nerdtree nmap <buffer> <Tab> :wincmd p<CR>

" отключаем переходы по CTRL+I/O в NERDTree
autocmd FileType nerdtree noremap <buffer> <C-O> <nop>
autocmd FileType nerdtree noremap <buffer> <C-I> <nop>

" выделение активного файла в дереве с возвратом фокуса
function! RCsyncTree(noFocus)
  if IsNTOpen() && !&diff && filereadable(expand('%'))
    NERDTreeFind
    if a:noFocus
      wincmd p
    endif
  endif
endfunction

" подсветка активного файла в дереве при перемещениях по буферам
autocmd BufWinEnter * :call RCsyncTree(1)

" по <F3> отображаем дерево тегов
map <F3> :TagbarToggle<CR>

" вставляет относительный путь выбранного файла в дереве NERDTree
function! NERDTreeInsertPath()
    if g:NERDTree.IsOpen()
        call NERDTreeFocus()
        let l:rcnode = g:NERDTreeFileNode.GetSelected()
        if l:rcnode != {}
            let rcpath = fnamemodify(l:rcnode.path.str(), ':.')
            wincmd p
            call setreg('"', rcpath, 'v')
            norm! ""p
        else
            wincmd p
        endif
    endif
endfunction

inoremap <C-X><C-N> <ESC>:call NERDTreeInsertPath()<CR>

" }}} NERDTREE

" {{{ ДОПОЛНИТЕЛЬНЫЕ ПЛАГИНЫ

let g:tagbar_iconchars = ['▸', '▾']

" фикс 'нажмите Enter ...' при открытом NerdTree
let g:rooter_silent_chdir = 1

" библиотеки функций
" tlib_vim
" vim-addon-mw-utils
" поддержка сниппетов
" vim-snipmate
" всевозможные сниппеты
" vim-snippets
" повтор команд от плагинов по '.'
" vim-repeat
" работа со скобками, тегами, кавычками и прочим в пару касаний
" vim-surround
" визуальные маркеры переходов
" vim-signature
" всевозможные текстовые объекты VIM
" targets.vim
" генераторы последовательностей
" Nexus

" }}} ДОПОЛНИТЕЛЬНЫЕ ПЛАГИНЫ

" русская раскладка по CTRL+^
set keymap=russian-jcukenwin

set iminsert=0
set imsearch=0

" меню выбора кодировки текущего файла по <F8>
set wildmenu
set wcm=<Tab>
menu Encoding.utf-8 :e ++enc=utf8 <CR>
menu Encoding.windows-1251 :e ++enc=cp1251 ++ff=dos <CR>
menu Encoding.cp866 :e ++enc=cp866 ++ff=dos <CR>
menu Encoding.ucs-2le :e ++enc=ucs-2le <CR>
menu Encoding.koi8-r :e ++enc=koi8-r ++ff=unix <CR>
map <F8> :emenu Encoding.<TAB>


set tabstop=4
set shiftwidth=4
set smarttab
set showmatch   " показывать первую парную скобку после ввода второй
set hlsearch
set incsearch
set ignorecase
set lazyredraw
set number
set listchars=tab:··
set list
set scrolloff=0 " сколько строк внизу и вверху экрана будут 'горячими'
set expandtab   " преобразовать табуляцию в пробелы
set colorcolumn=79
set shellslash
set mouse=a
set mousemodel=popup

set background=dark
colorscheme wombat256grf

" исключительные параметры табуляции для некоторых видов файлов
autocmd FileType crontab,fstab,make set noexpandtab tabstop=8 shiftwidth=8

" проверка орфографии по <F11>
if version >= 700 " изначально проверка орфографии выключена.
    setlocal spell spelllang=
    setlocal nospell
    function ChangeSpellLang()
        if &spelllang =~ "en_us"
            setlocal spell spelllang=ru
            echo "spelllang: ru"
        elseif &spelllang =~ "ru"
            setlocal spell spelllang=
            setlocal nospell
            echo "spelllang: off"
        else
            setlocal spell spelllang=en_us
            echo "spelllang: en"
        endif
    endfunc
    " переключаем проверку орфографии по <F11> в цикле en->ru->off
    map <F11> <Esc>:call ChangeSpellLang()<CR>
endif

set statusline=%<%f%h%m%r%=MODE\(%{&fileformat},%{&fileencoding}\)\ %b\ 0x%B\ %l,%c%V\ %P
set laststatus=2

" Устанавливаем регистр подсветки (@/) на текущее слово и включаем подсветку
" по двойному клику мышки 
nnoremap <silent> <2-LeftMouse> :let @/='\V\<'.escape(expand('<cword>'), '\').'\>'<cr>:set hls<cr>

" Поиск и подсветка выделленого текста
vnoremap <silent> <F3> y<esc>:let @/='\V'.escape(@", '\')<cr>:set hls<cr>


function! EnterFileDir(restore)
    if a:restore
        let savedpwd = getbufvar('%', 'savedpwd')
        if !empty(savedpwd) && savedpwd !=# getcwd()
            execute ':lcd' fnameescape(savedpwd)
        endif
    else
        call setbufvar('%', 'savedpwd', getcwd())
        lcd %:p:h
    endif
endfunction

" авто дополнение путей по <C-X><C-F> вызывает переход в директорию открытого
" файла для дополнения путей относительно текущего файла
inoremap <C-X><C-F> <C-O>:call EnterFileDir(0)<CR><C-X><C-F>

" авто-переход в директорию открытого файла при редактировании
" autocmd InsertEnter * call EnterFileDir(0)

" восстановление рабочей директории по завершении ввода
autocmd InsertLeave * call EnterFileDir(1)

" понижаем задержку ввода escape последовательностей (зачем?)
set ttimeoutlen=10

" смена курсора в зависимости от режима
let &t_SI.="\e[5 q" " SI = режим вставки
let &t_SR.="\e[3 q" " SR = режим замены
let &t_EI.="\e[1 q" " EI = нормальный режим
" 1 - это мигающий прямоугольник
" 2 - обычный прямоугольник
" 3 - мигающее подчёркивание
" 4 - просто подчёркивание
" 5 - мигающая вертикальная черта
" 6 - просто вертикальная черта


" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)

" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

source <sfile>:h/extra.vim

